<?php

/**
 * @file
 * BrontoAPI -- method for Drupal modules to send email via the Bronto API.
 *
 * Please refer to API.txt for how to write a module that uses this API.
 */

define("BRONTOAPI_WSDL", 'https://api.bronto.com/v4?wsdl');
define("BRONTOAPI_URL", 'https://api.bronto.com/v4');

/**
 * Implements hook_menu().
 */
function brontoapi_menu() {
  $items = array();

  $items['admin/config/brontoapi/settings'] = array(
    'title' => 'Bronto API Settings',
    'description' => 'Settings for Bronto integration',
    'type' => MENU_NORMAL_ITEM,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('brontoapi_admin_form'),
    'access arguments' => array('administer bronto'),
    'file' => 'brontoapi.admin.inc',
  );

  $items['brontoapi/messages'] = array(
    'title' => 'Autocomplete for Bronto API',
    'page callback' => '_brontoapi_message_autocomplete',
    'access arguments' => array('use autocomplete'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function brontoapi_permission() {
  return array(
    'administer bronto' => array(
      'title' => t('Administer Bronto API Settings'),
    ),
  );
}

/**
 * Business function to actually write the SOAP request to Bronto.
 */
function brontoapi_mail_message($to, $message_id, $replacements = array(), $extras = array()) {
  global $base_url;
  // For intelligent default
  // Overwrite $to to honor test mode.
  if (variable_get('brontoapi_test_mode', 0)) {
    $to = array(variable_get('brontoapi_test_list', ''));
  }

  $from_email = isset($extras['from']) ? $extras['from'] : variable_get('brontoapi_default_from', '');
  $reply_email = isset($extras['reply']) ? $extras['reply'] : $from_email;

  if (!is_array($to)) {
    $to = array($to);
  }

  $recipients = array();
  foreach ($to as $recipient) {
    if (strpos($recipient, '@') > 1) {
      $recipients[] = array(
        'type' => 'contact',
        'id' => brontoapi_get_or_create_contact($recipient),
      );
    }
  }

  $now = date('c');
  $deliveries = array(
    'start' => $now,
    'messageId' => $message_id,
    'type' => 'triggered',
    'fromEmail' => $from_email,
    'replyEmail' => $reply_email,
    'recipients' => $recipients,
    'fromName' => variable_get("brontoapi_default_from_name", ""),
  );

  if (!empty($replacements)) {
    foreach ($replacements as $replacement) {
      $deliveries['fields'][] = array(
        'name' => $replacement['name'],
        'type' => $replacement['type'],
        'content' => $replacement['content'],
      );
    }
  }

  if (isset($extras['reply_name'])) {
    $deliveries['fromName'] = $extras['reply_name'];
  }

  $ret = brontoapi_exec('addDeliveries', array($deliveries));
  if ($ret->results->isError == 1) {
    return FALSE;
  }
  return $ret;
}

/**
 * Reads fields from Bronto API.
 */
function brontoapi_read_fields() {
  $ret = brontoapi_exec_all(
      "readFields", array(
        "filter" => array(),
      ));
  return $ret;
}

/**
 * Function to return a contact ID. Used internally.
 */
function brontoapi_get_or_create_contact($email) {
  $ret = brontoapi_get_contact_id($email);
  if (!$ret) {
    $ret = brontoapi_add_email($email);
  }
  return $ret;
}

/**
 * Function to return a contactObject. Used internally.
 */
function brontoapi_get_or_create_contact_obj($email, $include_lists = FALSE) {
  $ret = brontoapi_get_contact_obj($email, $include_lists);
  if (!$ret) {
    $ret = brontoapi_add_email_get_obj($email);
  }
  return $ret;
}

/**
 * SOAP wrapper to look up a contact. Returns a contactObject.
 */
function brontoapi_get_contact_obj($email, $include_lists = FALSE) {
  $res = brontoapi_exec_all('readContacts', array(
    'filter' => array(
      'email' => array(
        array(
          'value' => $email,
          'operator' => 'EqualTo',
        ),
      ),
    ),
    'includeLists' => $include_lists,
  ));

  if (count($res) > 1) {
    brontoapi_error(t('Multiple contacts for one email address found. Contact Bronto.'));
  }
  else {
    $res = reset($res);
  }
  return $res;
}

/**
 * SOAP wrapper to look up a contact ID.
 */
function brontoapi_get_contact_id($email) {
  $ret = brontoapi_get_contact_obj($email);
  if (!$ret) {
    return FALSE;
  }
  return $ret->id;
}

/**
 * SOAP wrapper to look up a field ID.
 */
function brontoapi_get_field_id($name) {
  $res = brontoapi_exec_all('readFields', array(
    'filter' => array(
      'name' => array('operator' => 'Equalto', 'value' => $name),
    ),
  ));
  if (count($res) > 1) {
    brontoapi_error(t('Multiple fields found for this field name.'));
    return FALSE;
  }
  else {
    $res = reset($res);
  }
  return $res->id;
}

/**
 * API function to add an email address to Bronto. Returns an object.
 */
function brontoapi_add_email_get_obj($email, $fields = NULL) {

  $field_ids = array();
  if ($fields) {
    if (!is_array($fields)) {
      $fields = array($fields);
    }
    if (!empty($fields)) {
      foreach ($fields as $field) {
        $field_ids = array(
          'fieldId' => $field->return->id,
          'content' => $field->return->content,
        );
      }
    }
  }

  if (!is_array($email)) {
    $email = array($email);
  }

  $contacts = array();
  foreach ($email as $e) {

    $contact = array(
      'msgPref' => 'html',
      'email' => $e,
      'customSource' => 'DrupalModule',
    );

    if (!empty($field_ids)) {
      $contact['fields'] = array($field_ids);
    }
    $contacts[] = $contact;
  }

  $res = brontoapi_exec('addContacts', $contacts);

  $count = count($res);
  if ($count > 1 || !isset($res) || $res->results->isError == 1) {
    return FALSE;
  }
  else {
    $res = reset($res);
  }
  return $res;
}

/**
 * API function to add an email address to Bronto. Returns id only.
 */
function brontoapi_add_email($email, $fields = NULL) {
  $res = brontoapi_get_or_create_contact_obj($email, $fields);
  return $res->id;
}

/**
 * SOAP wrapper to get a list of message folders.
 */
function brontoapi_get_all_message_folders() {
  $data = &drupal_static(__FUNCTION__);
  if (!isset($data)) {
    if ($cache = cache_get('brontoapi_folder_data') && !empty($cache->data)) {
      $data = $cache->data;
    }
    else {
      $ret = brontoapi_exec_all('readMessageFolders', array('filter' => array()));
      if ($ret == FALSE) {
        return FALSE;
      }
      // Convert return to something more sensible.
      foreach ($ret as $result) {
        $data[$result->name] = $result->id;
      }
    }
    cache_set('brontoapi_folder_data', $data, 'cache');
  }
  return $data;
}

/**
 * Low-level SOAP implementor which sends a message to api.bronto.com.
 */
function brontoapi_exec($func, $args) {

  if (FALSE === ($binding = brontoapi_session())) {
    return FALSE;
  }
  try {
    $result = $binding->$func($args);
    if (is_object($result) && !empty($result->return)) {
      return $result->return;
    }
    else {
      return FALSE;
    }
  }

  catch (Exception $e) {
    brontoapi_error(t('Server Error:') . " " . $e->getMessage());
    return FALSE;
  }
}

/**
 * Bronto API exec that continues to execute query while objects are returned.
 *
 * @return array
 *   An array of Bronto API return objects, in no particular order.
 */
function brontoapi_exec_all($func, $args) {
  $result = array();
  $page = 1;
  do {
    $args['pageNumber'] = $page;
    $ret = brontoapi_exec($func, $args);
    if (empty($ret)) {
      break;
    }
    if (is_array($ret)) {
      $result = array_merge($result, $ret);
    }
    else {
      $result[] = $ret;
    }
    ++$page;
  } while (!empty($ret));

  return $result;
}

/**
 * Function to login to Bronto. Caches session id if possible.
 */
function brontoapi_session() {
  static $local_session = FALSE;

  if ($local_session) {
    return $local_session;
  }

  $binding = new SoapClient(BRONTOAPI_WSDL, array(
    'trace' => 1,
    'encoding' => 'UTF-8',
  ));
  try {
    $parameters = array(
      'apiToken' => variable_get('brontoapi_token', ''),
    );

    $result = $binding->login($parameters);

    if (!$result->return) {
      brontoapi_error(t('Could not connect to Email server'));
      return FALSE;
    }

    $session_data['session_id'] = $result->return;
    $session_data['__last_used'] = time();
    variable_set("brontoapi_session_data", $session_data);
  }
  catch (SoapFault $exception) {
    brontoapi_error(t('Could not connect to Email server'));
    return FALSE;
  }

  $session_header = new SoapHeader('http://api.bronto.com/v4', 'sessionHeader', array('sessionId' => $session_data['session_id']));
  $binding->__setLocation(BRONTOAPI_URL);
  $binding->__setSoapHeaders(array($session_header));

  $local_session = $binding;

  return $binding;
}

/**
 * SOAP will return one object instead of an array which will break loops.
 */
function brontoapi_make_array($obj_or_array) {
  if (is_object($obj_or_array)) {
    return array($obj_or_array);
  }
  else {
    return $obj_or_array;
  }
}

/**
 * Similar to brontoapi_get_all_messages() except this function with prefixes.
 *
 * @return array
 *   An array of bronto messages
 */
function brontoapi_get_all_messages_by_folder() {
  $data = &drupal_static(__FUNCTION__);
  if (!isset($data)) {
    if ($cache = cache_get('brontoapi_messages_folder_data') && !empty($cache->data)) {
      $data = $cache->data;
    }
    else {
      $folders = brontoapi_get_all_message_folders();
      if (empty($folders)) {
        return FALSE;
      }

      foreach ($folders as $name => $id) {
        $ret = brontoapi_exec_all("readMessages", array(
          "includeContent" => FALSE,
          "filter" => array("messageFolderId" => $id),
        ));

        foreach ($ret as $result) {
          if (!empty($result->status) && $result->status == "active") {
            $data[$name . ': ' . $result->name] = $result->id;
          }
        }
      }
      cache_set('brontoapi_messages_folder_data', $data, 'cache');
    }
  }
  return $data;
}

/**
 * Gets all Bronto message ids.
 */
function brontoapi_get_all_messages() {
  $data = &drupal_static(__FUNCTION__);
  if (!isset($data)) {
    if ($cache = cache_get('brontoapi_messages_data') && !empty($cache->data)) {
      $data = $cache->data;
    }
    else {
      $ret = brontoapi_exec_all("readMessages", array(
        "includeContent" => FALSE,
        "filter" => array(),
      ));
      foreach ($ret as $result) {
        if ($result->status == "active") {
          $data[$result->name] = $result->id;
        }
      }
      cache_set('brontoapi_messages_data', $data, 'cache');
    }
  }
  return $data;
}

/**
 * Error message storage and retrieval function.
 */
function brontoapi_error($new_message = FALSE) {
  static $message = FALSE;
  if ($new_message) {
    $message = $new_message;
  }
  return $message;
}

/**
 * Implements hook_form_alter().
 */
function brontoapi_form_alter(&$form, &$form_state, $form_id) {
  // Default implementation for core email templates.
  static $brontoapi_drupal_template_info;
  if (!isset($brontoapi_drupal_template_info)) {
    if ($cache = cache_get('brontoapi_drupal_template_info') && !empty($cache->data)) {

      $brontoapi_drupal_template_info = unserialize($cache->data);
    }
    else {
      $brontoapi_drupal_template_info = brontoapi_drupal_template_info();
    }
  }

  if (array_key_exists($form_id, $brontoapi_drupal_template_info)) {
    // Get all the message options.
    $messages = brontoapi_get_all_messages_by_folder();
    $options = array();
    foreach ($messages as $name => $data) {
      $options[$data] = $name;
    }
    $options += array("none" => "None");

    array_unshift($form['#submit'], 'brontoapi_form_submit');
    if (count($options) < 20 || TRUE) {
      foreach ($brontoapi_drupal_template_info[$form_id] as $form_item) {
        if (!empty($form_item['callback'])) {
          $form_item['callback']($form_item, $form);
        }
        else {
          if (!empty($form_item['container'])) {
            $form_container = &$form[$form_item['container']];
          }
          else {
            $form_container = &$form;
          }
          if (!empty($form_item['element_create'])) {
            if (is_array($form)) {
              $element = $form_item['element_create']($form_item, $form, $form_id);
            }
          }
          else {
            $element = brontoapi_element_create($form_item['machine_name'], $form_item['container']);
          }
          foreach ($form_item['elements'] as $template) {
            // Check if the element already has something to toggle visibility,
            // if so, add Bronto API visibility settings on top of it.
            if (isset($form_container[$template]['#states']['visible'])) {
              $form_container[$template]['#states']['visible'] = array_merge($form_container[$template]['#states']['visible'], array(
                'select[name="' . $element . '"]' => array('value' => 'none'),
              ));
            }
            $form_container[$template]['#states'] = array(
              'visible' => array(
                'select[name="' . $element . '"]' => array('value' => 'none'),
              ),
            );
          }
          $form_container[$element] = array(
            "#type" => "select",
            "#title" => "Select Bronto Template",
            "#options" => $options,
            '#default_value' => isset($element) ? brontoapi_get_message_name(variable_get($element, "")) : "",
          );
        }
      }
    }
    if (count($options) > 20) {
      foreach ($brontoapi_drupal_template_info[$form_id] as $form_item) {
        if (!empty($form_item['callback'])) {
          $form_item['callback']($form_item, $form);
        }
        else {
          if (!empty($form_item['container'])) {
            $form_container = &$form[$form_item['container']];
          }
          else {
            $form_container = &$form;
          }
          if (!empty($form_item['element_create'])) {
            if (is_array($form)) {
              $element = $form_item['element_create']($form_item, $form, $form_id);
            }
          }
          else {
            $element = brontoapi_element_create($form_item['machine_name'], $form_item['container']);
          }
          foreach ($form_item['elements'] as $template) {
            // Check if the element already has something to toggle visibility,
            // if so, add Bronto API visibility settings on top of it.
            if (isset($form_container[$template]['#states']['visible'])) {
              $form_container[$template]['#states']['visible'] = array_merge($form_container[$template]['#states']['visible'], array(
                'input[name="' . $element . '"]' => array('filled' => FALSE),
              ));
            }
            $form_container[$template]['#states'] = array(
              'visible' => array(
                'input[name="' . $element . '"]' => array('filled' => FALSE),
              ),
            );
          }

          $val = variable_get($element, "");

          $form_container[$element] = array(
            "#type" => 'textfield',
            '#title' => "Select Bronto Template",
            "#maxlength" => 128,
            "#autocomplete_path" => "brontoapi/messages",
            '#default_value' => isset($val) ? brontoapi_get_message_name($val) . " ($val)" : "",
            '#element_validate' => array('_brontoapi_autocomplete_validate'),
          );
        }
      }
    }
  }
}

/**
 * Translates message names to ids.
 */
function brontoapi_form_submit($form, &$form_state) {
  $values = &$form_state['values'];
  $messages = brontoapi_get_all_messages_by_folder();
}

/**
 * Gets the message name based on the message id.
 */
function brontoapi_get_message_name($message_id) {
  if (empty($message_id)) {
    return "";
  }
  $messages = brontoapi_get_all_messages();
  $messages = array_flip($messages);
  if (!isset($messages[$message_id])) {
    return "";
  }
  return $messages[$message_id];
}

/**
 * Creates an element on a form provided in brontoapi_drupal_template_info().
 */
function brontoapi_element_create($machine_name, $container = '') {
  $element = $machine_name . "_" . $container . "_" . '_brontoapi';
  return $element;
}

/**
 * Defines core email template changes as well as an alter hook.
 */
function brontoapi_drupal_template_info() {
  $brontoapi_drupal_template_info = array(
    "user_admin_settings" => array(
      array(
        "container" => "email_pending_approval",
        "id" => "user_register_pending_approval",
        "machine_name" => "email_pending_approval",
        "label" => "User Register Pending Approval",
        "elements" => array(
          "user_mail_register_pending_approval_subject",
          "user_mail_register_pending_approval_body",
        ),
      ),
      array(
        "container" => "email_pending_approval",
        "id" => "user_register_pending_approval_admin",
        "machine_name" => "email_pending_approval",
        "label" => "User Register Pending Admin Approval",
        "elements" => array(
          "user_mail_register_pending_approval_subject",
          "user_mail_register_pending_approval_body",
        ),
      ),
      array(
        "container" => "email_admin_created",
        "id" => "user_register_admin_approval_admin",
        "machine_name" => "email_admin_created",
        "label" => "User Register Admin Approval Email",
        "elements" => array(
          "user_mail_register_admin_created_subject",
          "user_mail_register_admin_created_body",
        ),
      ),
      array(
        "container" => "email_no_approval_required",
        "id" => "user_register_no_approval_required",
        "machine_name" => "email_no_approval_required",
        "label" => "Approval with No Approval Required",
        "elements" => array(
          "user_mail_register_no_approval_required_subject",
          "user_mail_register_no_approval_required_body",
        ),
      ),
      array(
        "container" => "email_password_reset",
        "id" => "user_register_password_reset",
        "machine_name" => "email_password_reset",
        "label" => "Email Password Reset",
        "elements" => array(
          "user_mail_password_reset_subject",
          "user_mail_password_reset_body",
        ),
      ),
      array(
        "container" => "email_activated",
        "id" => "user_register_activated",
        "machine_name" => "email_activated",
        "label" => "Email Activated",
        "elements" => array(
          "settings",
        ),
      ),
      array(
        "container" => "email_blocked",
        "id" => "user_register_blocked",
        "machine_name" => "email_blocked",
        "label" => "Email Blocked",
        "elements" => array(
          "settings",
        ),
      ),
      array(
        "container" => "email_cancel_confirm",
        "id" => "user_register_cancel_confirm",
        "machine_name" => "email_cancel_confirm",
        "label" => "Email Confirm Cancel",
        "elements" => array(
          "user_mail_cancel_confirm_subject",
          "user_mail_cancel_confirm_body",
        ),
      ),
      array(
        "container" => "email_canceled",
        "id" => "user_register_canceled",
        "machine_name" => "email_canceled",
        "label" => "Email Canceled",
        "elements" => array(
          "settings",
        ),
      ),
    ),
  );
  drupal_alter("brontoapi_drupal_template_info", $brontoapi_drupal_template_info);
  return $brontoapi_drupal_template_info;
}

/**
 * Gets all message ids and iterates over them to produce a list for selection.
 */
function _brontoapi_message_autocomplete($string = '') {
  if ($string) {
    $matches = array();
    $results = brontoapi_get_all_messages_by_folder();
    foreach ($results as $result => $id) {
      if (strpos($result, $string) !== FALSE) {
        $key = $result . " ($id)";
        $matches[$key] = check_plain($result);
      }
    }
    drupal_json_output($matches);
  }
}

/**
 * Implements hook_mail_alter().
 */
function brontoapi_mail_alter(&$message) {
  $containers = brontoapi_drupal_template_info();
  foreach ($containers as $templates) {
    foreach ($templates as $template) {
      if ($message['id'] == $template['id']) {
        $message['send'] = FALSE;
        $machine_name = variable_get($template['machine_name'] . "_" . $template['container'] . "_" . "_brontoapi");
        if (!empty($machine_name)) {
          $message['bronto_template'] = $machine_name;
          $user = user_load_by_mail($message['to']);
          $result = brontoapi_mail_message($message['to'], $message['bronto_template'], array());
          if (empty($result)) {
            watchdog("BRONTO MAILSYSTEM ERROR", "<pre>" . print_r($message, TRUE) . "</pre>");
          }
        }
      }
    }
  }
}

/**
 * Matches the message id value for autocomplete.
 */
function _brontoapi_autocomplete_validate($element, &$form_state, $form) {
  // If a value was entered into the autocomplete...
  $value = '';
  if (!empty($element['#value'])) {
    // Take "label (entity id)', match the id from parenthesis.
    if (preg_match("/.+\((\w+)\)/", $element['#value'], $matches)) {
      $value = $matches[1];
    }
  }
  // Update the value of this element so the field can validate the product IDs.
  form_set_value($element, $value, $form_state);
}
